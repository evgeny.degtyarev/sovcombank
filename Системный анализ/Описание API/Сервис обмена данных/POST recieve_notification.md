# Получение уведомлений

## Назначение
Метод для асинхронной интеграции в качестве развития MVP

Для передачи сообщений используется протокол JSON-RPC 2.0

## Параметры заголовка
Параметр                                        |Значение
---                                             |---
POST /recieve_notification                      |Тип запроса и адрес партнёра
API-key                                         |API-ключ. GUID-идентификатор
Content-Type                                    |application/json-rpc; charset=utf-8

## Параметры запроса

### Обязательные атрибуты
Параметр            |Значение
---                 |---
jsonrpc             |"2.0". Является константой, определяет версию протокола JSON-RPC
method              |"Подтверждение о получении"
params              |"Принято"
id                  |Идентификатор транзакции transaction_guid


## Результат
JSON-файл с идентификатором транзакции и отметкой об успешности                                                 


## Примеры
### Пример запроса "Подтверждение о получении"
```
HTTP/1.1 200 OK
Content-Type: application/json-rpc;charset=utf-8
API-key: {API-key}
{
	"jsonrpc": "2.0",
	"method": "Подтверждение о получении",
	"params": "Принято",
	"id": "{transaction_guid}"
}
```

### Пример ответа
```
HTTP/1.1 200 OK
Content-Type: application/json-rpc;charset=utf-8
API-key: {API-key}
{
	"jsonrpc": "2.0",
	"result": "Принято",
	"id": "{transaction_guid}"
}
```
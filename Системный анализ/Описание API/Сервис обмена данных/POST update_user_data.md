# Получение данных

## Назначение
Для передачи сообщений используется протокол JSON-RPC 2.0

## Параметры заголовка
Параметр                                        |Значение
---                                             |---
POST /recieve_notification                      |Тип запроса и адрес партнёра
API-key                                         |API-ключ. GUID-идентификатор
Content-Type                                    |application/json-rpc; charset=utf-8

## Параметры запроса

### Обязательные атрибуты
Параметр                  |Значение
---                       |---
jsonrpc                   |"2.0". Является константой, определяет версию протокола JSON-RPC
method                    |Название метода: "Выдана карта", "Новое событие"
params.payment_data_hash  |Идентификатор клиента -- SHA-256 хэш платёжных данных карты (номер карты + срок действия + cvc-код)
params.event              |Тип события: <br>- opened <br>- closed <br>- threshold_2k_exceed
id                        |Идентификатор транзакции transaction_guid


## Результат
JSON-файл с идентификатором транзакции и отметкой об успешности                                                 


## Примеры
### Пример запроса "Выдана карта"
```
HTTP/1.1 200 OK
Content-Type: application/json;charset=utf-8
API-key: {API-key}
{
	"jsonrpc": "2.0",
	"method": "Выдана карта",
	"params":
	    {
            "payment_data_hash": "{payment_data_hash}",
            "event": "opened"
        }
	"id": "{transaction_guid}"    
}
```

### Пример запроса "Новое событие"
```
HTTP/1.1 200 OK
Content-Type: application/json-rpc;charset=utf-8
API-key: {API-key}
{
	"jsonrpc": "2.0",
	"method": "Новое событие",
	"params":
	    {
            "payment_data_hash": "{payment_data_hash}",
            "event": "{closed|threshold_2k_exceed}"
         }
	"id": "{transaction_guid}"           
}
```


### Пример ответа
```
HTTP/1.1 200 OK
Content-Type: application/json-rpc;charset=utf-8
API-key: {API-key}
{
	"jsonrpc": "2.0",
	"result": "Принято",
	"id": "{transaction_guid}"
}
```